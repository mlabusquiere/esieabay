package fr.esiea.poo.esieabay.util;

import fr.esiea.poo.esieabay.core.dao.factory.DaoFactory;

public class utilTest {

	public static void resetDatabases() {
		DaoFactory.getUserDao().reset();
		DaoFactory.getBidDao().reset();
	}

}

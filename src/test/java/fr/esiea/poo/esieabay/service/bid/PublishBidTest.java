package fr.esiea.poo.esieabay.service.bid;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.esiea.poo.esieabay.core.dao.IDao;
import fr.esiea.poo.esieabay.core.dao.factory.DaoFactory;
import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.NoExistException;
import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.exception.PublishedException;
import fr.esiea.poo.esieabay.core.exception.WrongFormatException;
import fr.esiea.poo.esieabay.core.service.IBidSellerService;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.composant.Date;
import fr.esiea.poo.esieabay.model.composant.Product;
import fr.esiea.poo.esieabay.model.composant.bidenum.BidState;
import fr.esiea.poo.esieabay.model.imodel.IBid;
import fr.esiea.poo.esieabay.model.implementation.Bid;
import fr.esiea.poo.esieabay.model.implementation.User;
import fr.esiea.poo.esieabay.util.utilTest;

public class PublishBidTest {

	private User userAuthenticated;
	private Bid bid;

	private IBidSellerService bidSellerService;
	private IDaoService<User> userService;
	
	@Before
	public void setup() throws WrongFormatException, NonUniqueException, AuthenticationException {

		userAuthenticated = new User("login","lastname","firstname");
		bid = new Bid(userAuthenticated, new Product("Description"), new Date(10));
		
		bidSellerService = ServiceFactory.getBidSellerService();
		userService = ServiceFactory.getUserService();
		
		userService.create(userAuthenticated);
		bidSellerService.makeBid(userAuthenticated, bid);
	}

	@After
	public void tearDown() {
		utilTest.resetDatabases();
	}
	
	/**
	 * l'enchere est elle publi�
	 * @throws NoExistException  
	 * @throws PublishedException 
	 * @throws AuthenticationException 
	 */
	@Test
	public void Published() throws NoExistException, PublishedException, AuthenticationException {
		IDao<IBid> bidDao = DaoFactory.getBidDao();
		bidSellerService.publishBid(userAuthenticated, bid.getId());
		assertEquals(BidState.PUBLISHED, bidDao.get(bid.getId()).getState());
	}

	/**
	 * On ne peut pas publier une ench�re qui n'existe pas
	 * @throws NoExistException  
	 * @throws PublishedException 
	 * @throws AuthenticationException 
	 */
	@Test(expected = NoExistException.class)
	public void notExisting() throws NoExistException, PublishedException, AuthenticationException {
		bidSellerService.publishBid(userAuthenticated, "falseId");
	}


	/**
	 * l'utilistaeur n'est pas le propri�t�aire de l'enchere
	 * @throws AuthenticationException
	 * @throws NoExistException
	 * @throws PublishedException
	 */
	@Test(expected = AuthenticationException.class)
	public void wrongUser() throws AuthenticationException, NoExistException, PublishedException {
		bidSellerService.publishBid(new User("otherOne", "lastname","firstname"), bid.getId());
	}
	
	/**
	 * Seul une enchere cr��e peut etre publi�e
	 * @throws AuthenticationException
	 * @throws NoExistException
	 * @throws PublishedException 
	 */
	@Test
	public void created() throws AuthenticationException, NoExistException, PublishedException {
		boolean check=false;
		bid.setState(BidState.CANCELLED);
		
		try {
			bidSellerService.publishBid(userAuthenticated, bid.getId());
		} catch (PublishedException e) {
			check = true;
		}
		
		assertEquals(true,check);
		check=false;
		
		bid.setState(BidState.FINISHED);
		try {
			bidSellerService.publishBid(userAuthenticated, bid.getId());
		} catch (PublishedException e) {
			check = true;
		}
		
		assertEquals(true,check);
		check=false;
		
		bid.setState(BidState.PUBLISHED);
		try {
			bidSellerService.publishBid(userAuthenticated, bid.getId());
		} catch (PublishedException e) {
			check = true;
		}
		
		assertEquals(true,check);

		bid.setState(BidState.CREATED);
		bidSellerService.publishBid(userAuthenticated, bid.getId());
	}

}


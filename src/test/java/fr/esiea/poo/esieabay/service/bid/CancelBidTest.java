package fr.esiea.poo.esieabay.service.bid;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.NoExistException;
import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.exception.NotPublishedException;
import fr.esiea.poo.esieabay.core.exception.OfferTooLowException;
import fr.esiea.poo.esieabay.core.exception.OwnBidException;
import fr.esiea.poo.esieabay.core.exception.PublishedException;
import fr.esiea.poo.esieabay.core.exception.UncancellableBidException;
import fr.esiea.poo.esieabay.core.exception.WrongFormatException;
import fr.esiea.poo.esieabay.core.service.IBidBuyerService;
import fr.esiea.poo.esieabay.core.service.IBidSellerService;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.composant.Date;
import fr.esiea.poo.esieabay.model.composant.Product;
import fr.esiea.poo.esieabay.model.composant.bidenum.BidState;
import fr.esiea.poo.esieabay.model.imodel.IBid;
import fr.esiea.poo.esieabay.model.implementation.Bid;
import fr.esiea.poo.esieabay.model.implementation.User;
import fr.esiea.poo.esieabay.util.utilTest;

public class CancelBidTest {

	private User userAuthenticated;
	private IBid bid;

	private IBidBuyerService bidBuyerService;
	private IBidSellerService bidSellerService;
	IDaoService<User> userService;
	@Before
	public void setup() throws WrongFormatException, NonUniqueException, AuthenticationException, NoExistException, PublishedException {
		userAuthenticated = new User("login","lastname","firstname");
		bid = new Bid(userAuthenticated, new Product("Description"), new Date(10));
		
		bidSellerService = ServiceFactory.getBidSellerService();
		bidBuyerService = ServiceFactory.getBidBuyerService();
		userService = ServiceFactory.getUserService();
		
		userService.create(userAuthenticated);
		bidSellerService.makeBid(userAuthenticated, bid);
		bidSellerService.publishBid(userAuthenticated, bid.getId());
	}

	@After
	public void tearDown() {
		utilTest.resetDatabases();
	}
	
	/**
	 * V�rifie si l'ench�re a bien �t� annul�
	 * @throws NoExistException
	 * @throws AuthenticationException
	 * @throws UncancellableBidException
	 */
	@Test
	public void cancelBid() throws NoExistException, AuthenticationException, UncancellableBidException {
		bidSellerService.cancelBid(userAuthenticated, bid.getId());
		assertEquals(BidState.CANCELLED, bidSellerService.getBid(userAuthenticated, bid.getId()).getState());
	}

	/**
	 * V�rifie si l'ench�re ne peut pas �tre annul�e
	 * @throws UncancellableBidException
	 * @throws NoExistException
	 * @throws AuthenticationException
	 * @throws PublishedException
	 * @throws OwnBidException
	 * @throws NotPublishedException
	 * @throws OfferTooLowException
	 */
	@Test(expected = UncancellableBidException.class)
	public void uncancellableBid() throws UncancellableBidException, NoExistException, AuthenticationException, PublishedException, OwnBidException, NotPublishedException, OfferTooLowException {
		bidBuyerService.makeOffer(new User("buyer", "l", "f"), bid.getId(), 10);
		bidSellerService.cancelBid(userAuthenticated, bid.getId());
	}
}

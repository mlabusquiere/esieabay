package fr.esiea.poo.esieabay.service;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.esiea.poo.esieabay.service.bid.BidTests;
import fr.esiea.poo.esieabay.service.dao.DaoTests;
import fr.esiea.poo.esieabay.service.warning.WarningTests;

@RunWith(Suite.class)
@SuiteClasses({BidTests.class, WarningTests.class, DaoTests.class})
public class Tests {

}

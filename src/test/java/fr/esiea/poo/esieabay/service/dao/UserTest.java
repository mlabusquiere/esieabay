package fr.esiea.poo.esieabay.service.dao;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.implementation.User;
import fr.esiea.poo.esieabay.util.utilTest;

public class UserTest {
	
	private User user;
	IDaoService<User> userService;
	@Before
	public void setup() {
		user = new User("login", "lastname", "firstname");
		userService = ServiceFactory.getUserService();
		
		try {
			userService.create(user);
		} catch (NonUniqueException e) {
			fail();
		}
	}

	@After
	public void tearDown() {
		utilTest.resetDatabases();
	}
	
	@Test(expected = NonUniqueException.class)
	public void unique() throws NonUniqueException {
		userService.create(user);
	}
}

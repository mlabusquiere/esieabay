package fr.esiea.poo.esieabay.service.bid;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.NoExistException;
import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.exception.NotPublishedException;
import fr.esiea.poo.esieabay.core.exception.OfferTooLowException;
import fr.esiea.poo.esieabay.core.exception.OwnBidException;
import fr.esiea.poo.esieabay.core.exception.PublishedException;
import fr.esiea.poo.esieabay.core.exception.UncancellableBidException;
import fr.esiea.poo.esieabay.core.exception.WrongFormatException;
import fr.esiea.poo.esieabay.core.service.IBidBuyerService;
import fr.esiea.poo.esieabay.core.service.IBidSellerService;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.composant.Date;
import fr.esiea.poo.esieabay.model.composant.Product;
import fr.esiea.poo.esieabay.model.imodel.IBuyer;
import fr.esiea.poo.esieabay.model.imodel.ISeller;
import fr.esiea.poo.esieabay.model.implementation.Bid;
import fr.esiea.poo.esieabay.model.implementation.User;
import fr.esiea.poo.esieabay.util.utilTest;

public class GetBidsTest {
	private User user1, user2, user3;
	private Bid bid1, bid2;

	private IBidSellerService bidSellerService;
	private IDaoService<User> userService;
	private IBidBuyerService bidBuyerService;

	@Before
	public void setup() throws WrongFormatException, NonUniqueException, AuthenticationException {
		user1 = new User("1","lastname","firstname");
		user2 = new User("2","lastname","firstname");
		user3 = new User("3","lastname","firstname");
		bid1 = new Bid(user1, new Product("Description1"), new Date(10));
		bid2 = new Bid(user2, new Product("Description2"), new Date(20));
		bid1.setReservePrice(20);

		bidSellerService = ServiceFactory.getBidSellerService();
		bidBuyerService  = ServiceFactory.getBidBuyerService();
		userService = ServiceFactory.getUserService();

		userService.create(user1);
		userService.create(user2);

		bidSellerService.makeBid(user1, bid1);
		bidSellerService.makeBid(user2, bid2);
	}

	@After
	public void tearDown() {
		utilTest.resetDatabases();
	}

	/**
	 * Vérifie la visibilité des bids
	 * @throws NoExistException
	 * @throws PublishedException
	 * @throws AuthenticationException
	 * @throws OwnBidException
	 * @throws NotPublishedException
	 * @throws OfferTooLowException
	 * @throws UncancellableBidException
	 */
	@Test
	public void getBids() throws NoExistException, PublishedException, AuthenticationException, OwnBidException, NotPublishedException, OfferTooLowException, UncancellableBidException  {


		assertEquals(1, bidSellerService.getBids((ISeller) user1).size());
		assertEquals(1, bidSellerService.getBids((ISeller) user2).size());
		assertEquals(0, bidSellerService.getBids((ISeller) user3).size());

		assertEquals(0, bidBuyerService.getBids((IBuyer) user3).size());

		bidSellerService.publishBid(user1, bid1.getId());
		
		assertEquals(1, bidBuyerService.getBids().size());
		assertEquals(0, bidBuyerService.getBids((IBuyer) user3).size());

		bidSellerService.publishBid(user2, bid2.getId());

		assertEquals(2, bidBuyerService.getBids().size());
		assertEquals(0, bidBuyerService.getBids((IBuyer) user3).size());
	
		bidBuyerService.makeOffer(user2,bid1.getId(), 10);
		
		assertEquals(1, bidBuyerService.getBids((IBuyer) user2).size());
		assertEquals(0, bidBuyerService.getBids((IBuyer) user3).size());
		// TODO setReservePrice
		
		bidSellerService.cancelBid(user1, bid1.getId()); 
		
		assertEquals(1, bidBuyerService.getBids((IBuyer) user2).size());
		assertEquals(0, bidBuyerService.getBids((IBuyer) user3).size());
		
	}
}

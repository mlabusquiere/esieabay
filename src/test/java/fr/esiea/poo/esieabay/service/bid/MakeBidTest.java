package fr.esiea.poo.esieabay.service.bid;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.exception.WrongFormatException;
import fr.esiea.poo.esieabay.core.service.IBidSellerService;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.composant.Date;
import fr.esiea.poo.esieabay.model.composant.Product;
import fr.esiea.poo.esieabay.model.composant.bidenum.BidState;
import fr.esiea.poo.esieabay.model.imodel.ISeller;
import fr.esiea.poo.esieabay.model.implementation.Bid;
import fr.esiea.poo.esieabay.model.implementation.User;
import fr.esiea.poo.esieabay.util.utilTest;


public class MakeBidTest {

	private ISeller userAuthenticated;
	private Bid bid;

	private IBidSellerService bidSellerService;
	private IDaoService<User> userService;
	
	@Before
	public void setup() throws WrongFormatException, NonUniqueException, AuthenticationException {
		userAuthenticated = new User("login","lastname","firstname");
		bid = new Bid(userAuthenticated, new Product("Description"), new Date(10));
		
		bidSellerService = ServiceFactory.getBidSellerService();
		userService = ServiceFactory.getUserService();

		userService.create((User)userAuthenticated);
		bidSellerService.makeBid(userAuthenticated, bid);
	}

	@After
	public void tearDown() {
		utilTest.resetDatabases();
	}
	
	/**
	 * On verifie que l'enchere a sa cr�ation a le statut de CREATED
	 * @throws NonUniqueException
	 */
	@Test
	public void createdState() throws NonUniqueException {
		assertEquals(BidState.CREATED, bid.getState());
	}

	/**
	 * On ne peut pas cr�er deux fois la m�me ench�re
	 * @throws NonUniqueException
	 * @throws WrongFormatException 
	 * @throws AuthenticationException 
	 */
	@Test(expected = NonUniqueException.class)
	public void unique() throws NonUniqueException, WrongFormatException, AuthenticationException {
		bidSellerService.makeBid(userAuthenticated, bid);
	}

	/**
	 * Une enchere est caract�ris� par une date limite (et un bon product)
	 * @throws NonUniqueException 
	 * @throws AuthenticationException 
	 */
	@Test
	public void wrongFormat() throws NonUniqueException, AuthenticationException {
		//No due date
		Product productType = new Product("description");
		Bid bidWrongFormat = new Bid(userAuthenticated, productType, null);

		boolean check = false;

		try {
			bidSellerService.makeBid(userAuthenticated, bidWrongFormat);
		} catch (WrongFormatException e) {
			check=true;
		}
		
		assertEquals(true, check);// we went in the catch

		check=false;

		//Product with a wrong format
		Date dueDate = new Date(10);
		productType = null;
		bidWrongFormat = new Bid(userAuthenticated,productType,dueDate);

		try {
			bidSellerService.makeBid(userAuthenticated, bidWrongFormat);
		} catch (WrongFormatException e) {
			check=true;
		} 
		assertEquals(true, check);// we went in the catch
	}


}
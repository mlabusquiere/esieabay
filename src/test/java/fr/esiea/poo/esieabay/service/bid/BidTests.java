package fr.esiea.poo.esieabay.service.bid;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({CancelBidTest.class, GetBidsTest.class, MakeBidTest.class, MakeOfferTest.class, PublishBidTest.class, UpdateBidTest.class, BidDueDateTest.class})
public class BidTests {

}

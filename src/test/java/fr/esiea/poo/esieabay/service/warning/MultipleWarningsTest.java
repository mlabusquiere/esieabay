package fr.esiea.poo.esieabay.service.warning;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.NoExistException;
import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.exception.NotPublishedException;
import fr.esiea.poo.esieabay.core.exception.OfferTooLowException;
import fr.esiea.poo.esieabay.core.exception.OwnBidException;
import fr.esiea.poo.esieabay.core.exception.PublishedException;
import fr.esiea.poo.esieabay.core.exception.WrongFormatException;
import fr.esiea.poo.esieabay.core.service.IBidBuyerService;
import fr.esiea.poo.esieabay.core.service.IBidSellerService;
import fr.esiea.poo.esieabay.core.service.IWarningService;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.composant.Date;
import fr.esiea.poo.esieabay.model.composant.Product;
import fr.esiea.poo.esieabay.model.imodel.IBuyer;
import fr.esiea.poo.esieabay.model.imodel.ISeller;
import fr.esiea.poo.esieabay.model.implementation.Bid;
import fr.esiea.poo.esieabay.model.implementation.User;
import fr.esiea.poo.esieabay.util.utilTest;

public class MultipleWarningsTest {
	
	private ISeller seller;
	private IBuyer buyer1, buyer2;
	private Bid bid, bid2;

	private IBidSellerService bidSellerService;
	private IBidBuyerService bidBuyerService;
	private IWarningService warningService;
	private IDaoService<User> userService;
	
	
	@Before
	public void setup() throws WrongFormatException, NonUniqueException, AuthenticationException, NoExistException, PublishedException {
		seller = new User("seller", "lastname", "firstname");
		buyer1 = new User("buyer1", "lastname", "firstname");
		buyer2 = new User("buyer2", "lastname", "firstname");
		
		bid = new Bid(seller, new Product("Description"), new Date(10));
		bid2 = new Bid(seller, new Product("Description2"), new Date(20));
		bid.setReservePrice(20);
		bid2.setReservePrice(200);
		
		bidSellerService = ServiceFactory.getBidSellerService();
		bidBuyerService = ServiceFactory.getBidBuyerService();
		userService = ServiceFactory.getUserService();
		warningService = ServiceFactory.getWarningService();

		userService.create((User)seller);
		bidSellerService.makeBid(seller, bid);
		bidSellerService.publishBid(seller, bid.getId());
		bidSellerService.makeBid(seller, bid2);
		bidSellerService.publishBid(seller, bid2.getId());
	}

	@After
	public void tearDown() {
		utilTest.resetDatabases();
	}
	
	
	/**
	 * Test avec diff�rents types d'alertes
	 * @throws NoExistException
	 * @throws AuthenticationException
	 * @throws PublishedException
	 * @throws OwnBidException
	 * @throws NotPublishedException
	 * @throws OfferTooLowException
	 */
	@Test
	public void multipleWarningsTest() throws NoExistException, AuthenticationException, PublishedException, OwnBidException, NotPublishedException, OfferTooLowException {
		assertEquals(0, buyer1.getWarnings().size());
		warningService.reservePriceWarning(buyer1, true, bid.getId());
		warningService.higherOfferWarning(buyer1, true, bid.getId());
		bidBuyerService.makeOffer(buyer1, bid.getId(), 5);
		assertEquals(0, buyer1.getWarnings().size());
		bidBuyerService.makeOffer(buyer2, bid.getId(), 10);
		assertEquals(1, buyer1.getWarnings().size());
		bidBuyerService.makeOffer(buyer2, bid.getId(), 25);
		assertEquals(3, buyer1.getWarnings().size());
	}
	
	/**
	 * Test avec plusieurs utilisateurs avec config d'alert diff�rente
	 * @throws NoExistException
	 * @throws AuthenticationException
	 * @throws PublishedException
	 * @throws OwnBidException
	 * @throws NotPublishedException
	 * @throws OfferTooLowException
	 */
	@Test
	public void complexWarningsTest() throws NoExistException, AuthenticationException, PublishedException, OwnBidException, NotPublishedException, OfferTooLowException {
		assertEquals(0, buyer1.getWarnings().size());
		warningService.reservePriceWarning(buyer1, true, bid.getId());
		warningService.higherOfferWarning(buyer1, true, bid.getId());
		
		warningService.higherOfferWarning(buyer1, true, bid2.getId());
		
		bidBuyerService.makeOffer(buyer1, bid.getId(), 1);
		bidBuyerService.makeOffer(buyer1, bid2.getId(), 1);
		bidBuyerService.makeOffer(buyer2, bid.getId(), 5);
		
		assertEquals(1, buyer1.getWarnings().size());
		
		bidBuyerService.makeOffer(buyer2, bid2.getId(), 10);
		
		assertEquals(2, buyer1.getWarnings().size());
		
		bidBuyerService.makeOffer(buyer1, bid.getId(), 10);
		bidBuyerService.makeOffer(buyer2, bid.getId(), 25);
		
		assertEquals(5, buyer1.getWarnings().size());
	}
}

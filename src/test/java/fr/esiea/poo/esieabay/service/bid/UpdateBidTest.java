package fr.esiea.poo.esieabay.service.bid;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.BidNotUpdatableException;
import fr.esiea.poo.esieabay.core.exception.NoExistException;
import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.exception.PublishedException;
import fr.esiea.poo.esieabay.core.exception.WrongFormatException;
import fr.esiea.poo.esieabay.core.service.IBidSellerService;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.composant.Date;
import fr.esiea.poo.esieabay.model.composant.Product;
import fr.esiea.poo.esieabay.model.imodel.ISeller;
import fr.esiea.poo.esieabay.model.implementation.Bid;
import fr.esiea.poo.esieabay.model.implementation.User;
import fr.esiea.poo.esieabay.util.utilTest;

public class UpdateBidTest {
	private ISeller userAuthenticated;
	private Bid bid;

	private IBidSellerService bidSellerService;
	private IDaoService<User> userService;
	
	@Before
	public void setup() throws WrongFormatException, NonUniqueException, AuthenticationException {
		userAuthenticated = new User("login","lastname","firstname");
		bid = new Bid(userAuthenticated, new Product("Description"), new Date(10));
		
		bidSellerService = ServiceFactory.getBidSellerService();
		userService = ServiceFactory.getUserService();

		userService.create((User)userAuthenticated);
		bidSellerService.makeBid(userAuthenticated, bid);
	}
	
	@After
	public void tearDown() {
		utilTest.resetDatabases();
	}
	
	/**
	 * V�rifie si les prix minimum et de r�serve sont prit en compte
	 * @throws AuthenticationException
	 * @throws BidNotUpdatableException
	 * @throws NoExistException
	 */
	@Test
	public void update() throws AuthenticationException, BidNotUpdatableException, NoExistException {
		bid.setMinPrice(10);
		bid.setReservePrice(100);
		bidSellerService.updateBid(userAuthenticated, bid);
		assertEquals(bidSellerService.getBid(userAuthenticated,bid.getId()).getMinPrice(), 10, 0);
		assertEquals(bidSellerService.getBid(userAuthenticated,bid.getId()).getReservePrice(), 100, 0);
	}
	
	/**
	 * V�rifie si on ne peut plus modifier si l'ench�re est publi�
	 * @throws NoExistException
	 * @throws PublishedException
	 * @throws AuthenticationException
	 * @throws BidNotUpdatableException
	 */
	@Test (expected = BidNotUpdatableException.class)
	public void unupdatable() throws NoExistException, PublishedException, AuthenticationException, BidNotUpdatableException {
		bidSellerService.publishBid(userAuthenticated, bid.getId());
		bidSellerService.updateBid(userAuthenticated, bid);
	}

}

package fr.esiea.poo.esieabay.service.warning;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({OfferWarningTest.class, ReserveWarningTest.class, CancelWarningTest.class, MultipleWarningsTest.class, SellerWarningTest.class, DisableWarningsTest.class, RemoveWarningTest.class})
public class WarningTests {

}

package fr.esiea.poo.esieabay.service.bid;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.BidNotUpdatableException;
import fr.esiea.poo.esieabay.core.exception.NoExistException;
import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.exception.NotPublishedException;
import fr.esiea.poo.esieabay.core.exception.OfferTooLowException;
import fr.esiea.poo.esieabay.core.exception.OwnBidException;
import fr.esiea.poo.esieabay.core.exception.PublishedException;
import fr.esiea.poo.esieabay.core.exception.WrongFormatException;
import fr.esiea.poo.esieabay.core.service.IBidBuyerService;
import fr.esiea.poo.esieabay.core.service.IBidSellerService;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.composant.Date;
import fr.esiea.poo.esieabay.model.composant.Product;
import fr.esiea.poo.esieabay.model.imodel.ISeller;
import fr.esiea.poo.esieabay.model.implementation.Bid;
import fr.esiea.poo.esieabay.model.implementation.User;
import fr.esiea.poo.esieabay.util.utilTest;


public class MakeOfferTest {

	private User seller, buyer;
	private Bid bid;

	private IBidSellerService bidSellerService;
	private IDaoService<User> userService;
	private IBidBuyerService bidBuyerService;
	
	@Before
	public void setup() throws WrongFormatException, NonUniqueException, AuthenticationException {
		seller = new User("seller", "lastname", "firstname");
		buyer = new User("buyer", "lastname", "firstname");
		
		bid = new Bid(seller, new Product("product"), new Date(100));
		
		bidSellerService = ServiceFactory.getBidSellerService();
		bidBuyerService  = ServiceFactory.getBidBuyerService();
		userService = ServiceFactory.getUserService();
		
		userService.create(seller);
		userService.create(buyer);
		
		bidSellerService.makeBid(seller, bid);
	}

	@After
	public void tearDown() {
		utilTest.resetDatabases();
	}
	
	/**
	 * V�rifie si les offres sont bien ajout�es
	 * @throws NoExistException
	 * @throws PublishedException
	 * @throws AuthenticationException
	 * @throws OwnBidException
	 * @throws NotPublishedException
	 * @throws OfferTooLowException
	 */
	@Test
	public void makeOffer() throws NoExistException, PublishedException, AuthenticationException, OwnBidException, NotPublishedException, OfferTooLowException {
		bidSellerService.publishBid(seller, bid.getId());
		assertEquals(bidSellerService.getBid((ISeller) seller, bid.getId()).getOffers().size(), 0);
		bidBuyerService.makeOffer(buyer, bid.getId(), 10);
		assertEquals(bidSellerService.getBid((ISeller) seller, bid.getId()).getOffers().size(), 1);
	}
	
	/**
	 * V�rifie si l'offre n'est pas en dessous du prix minimum
	 * @throws OfferTooLowException
	 * @throws NoExistException
	 * @throws PublishedException
	 * @throws AuthenticationException
	 * @throws OwnBidException
	 * @throws NotPublishedException
	 * @throws BidNotUpdatableException
	 */
	@Test(expected = OfferTooLowException.class)
	public void offerTooLow() throws OfferTooLowException, NoExistException, PublishedException, AuthenticationException, OwnBidException, NotPublishedException, BidNotUpdatableException {
		bid.setMinPrice(20);
		bidSellerService.updateBid(seller, bid);
		bidSellerService.publishBid(seller, bid.getId());
		bidBuyerService.makeOffer(buyer, bid.getId(), 10);
	}

	/**
	 * On ne peut enchererir que sur une enchere publi�
	 * @throws AuthenticationException 
	 * @throws NoExistException  
	 * @throws PublishedException 
	 * @throws OwnBidException 
	 * @throws OfferTooLowException 
	 */
	@Test(expected = NotPublishedException.class)
	public void notPublished() throws NotPublishedException, NoExistException, AuthenticationException, PublishedException, OwnBidException, OfferTooLowException {
		bidBuyerService.makeOffer(buyer, bid.getId(), 10);
	}


	/**
	 * l'utilistaeur n'est pas le propri�t�aire de l'enchere
	 * @throws AuthenticationException 
	 * @throws NoExistException 
	 * @throws PublishedException 
	 * @throws NotPublishedException 
	 * @throws OfferTooLowException 
	 */
	@Test(expected = OwnBidException.class)
	public void wrongUser() throws OwnBidException, NoExistException, AuthenticationException, PublishedException, NotPublishedException, OfferTooLowException {
		bidBuyerService.makeOffer(seller,bid.getId(), 10);
	}
}

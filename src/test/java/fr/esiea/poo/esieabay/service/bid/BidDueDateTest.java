package fr.esiea.poo.esieabay.service.bid;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.NoExistException;
import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.exception.NotPublishedException;
import fr.esiea.poo.esieabay.core.exception.OfferTooLowException;
import fr.esiea.poo.esieabay.core.exception.OwnBidException;
import fr.esiea.poo.esieabay.core.exception.PublishedException;
import fr.esiea.poo.esieabay.core.exception.WrongFormatException;
import fr.esiea.poo.esieabay.core.service.IBidBuyerService;
import fr.esiea.poo.esieabay.core.service.IBidSellerService;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.composant.Date;
import fr.esiea.poo.esieabay.model.composant.Offer;
import fr.esiea.poo.esieabay.model.composant.Product;
import fr.esiea.poo.esieabay.model.composant.bidenum.BidState;
import fr.esiea.poo.esieabay.model.imodel.IBuyer;
import fr.esiea.poo.esieabay.model.imodel.ISeller;
import fr.esiea.poo.esieabay.model.implementation.Bid;
import fr.esiea.poo.esieabay.model.implementation.User;
import fr.esiea.poo.esieabay.util.utilTest;

public class BidDueDateTest {

	private ISeller seller;
	private IBuyer buyer1, buyer2;
	private Bid bid;

	private IBidSellerService bidSellerService;
	private IBidBuyerService bidBuyerService;
	private IDaoService<User> userService;
	
	@Before
	public void setup() throws WrongFormatException, NonUniqueException, AuthenticationException {
		seller = new User("seller", "lastname", "firstname");
		buyer1 = new User("buyer1", "lastname", "firstname");
		buyer2 = new User("buyer2", "lastname", "firstname");
		bid = new Bid(seller, new Product("Description"), new Date(10));
		
		bidSellerService = ServiceFactory.getBidSellerService();
		bidBuyerService = ServiceFactory.getBidBuyerService();
		userService = ServiceFactory.getUserService();

		userService.create((User)seller);
		bidSellerService.makeBid(seller, bid);
	}

	@After
	public void tearDown() {
		utilTest.resetDatabases();
	}
	
	/**
	 * On verifie si l'�tat de l'ench�re est bien mis � jour que la date limite est d�pass�
	 * @throws NonUniqueException
	 * @throws AuthenticationException 
	 * @throws PublishedException 
	 * @throws NoExistException 
	 */
	@Test
	public void dueDate() throws NonUniqueException, NoExistException, PublishedException, AuthenticationException {
		bidSellerService.publishBid(seller, bid.getId());
		bid.getDueDate().setActualDate(new Date(15));
		assertEquals(BidState.FINISHED, bid.getState());
	}

	
	/**
	 * V�rifie l'offre gagnante
	 * @throws NonUniqueException
	 * @throws NoExistException
	 * @throws PublishedException
	 * @throws AuthenticationException
	 * @throws OwnBidException
	 * @throws NotPublishedException
	 * @throws OfferTooLowException
	 */
	@Test
	public void winnerOffer() throws NonUniqueException, NoExistException, PublishedException, AuthenticationException, OwnBidException, NotPublishedException, OfferTooLowException {
		bidSellerService.publishBid(seller, bid.getId());
		bidBuyerService.makeOffer(buyer1, bid.getId(), 1);
		bidBuyerService.makeOffer(buyer2, bid.getId(), 5);
		bidBuyerService.makeOffer(buyer1, bid.getId(), 10);
		bidBuyerService.makeOffer(buyer2, bid.getId(), 15);
		bidBuyerService.makeOffer(buyer1, bid.getId(), 20);
		bid.getDueDate().setActualDate(new Date(15));
		assertEquals(BidState.FINISHED, bid.getState());
		assertEquals(new Offer(20, buyer1), bid.getMaxOffer());
	}
}
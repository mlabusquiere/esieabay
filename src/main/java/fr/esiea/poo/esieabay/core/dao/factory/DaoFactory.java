package fr.esiea.poo.esieabay.core.dao.factory;

import fr.esiea.poo.esieabay.core.dao.IDao;
import fr.esiea.poo.esieabay.daofake.DaoImp;
import fr.esiea.poo.esieabay.model.imodel.IBid;
import fr.esiea.poo.esieabay.model.implementation.User;

public class DaoFactory {
	private static DaoImp<User> daoImpUser;
	private static DaoImp<IBid> daoImpBid; 
	
	public static IDao<User> getUserDao() {
		if(daoImpUser == null)
			daoImpUser = new DaoImp<User>();
		return daoImpUser;
	}

	public static IDao<IBid> getBidDao() {
		if(daoImpBid == null)
			daoImpBid = new DaoImp<IBid>();
		return daoImpBid;
	}

}


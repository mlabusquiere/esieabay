package fr.esiea.poo.esieabay.core.dao;

import java.util.Collection;



public interface IDao<T> {

	T get(String id);

	void create(T arg0);

	void update(T arg0);
	
	Collection<T> get();
	//Only use for test
	void reset();

}

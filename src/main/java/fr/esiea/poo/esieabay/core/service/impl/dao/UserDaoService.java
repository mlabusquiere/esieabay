package fr.esiea.poo.esieabay.core.service.impl.dao;

import java.util.Collection;

import fr.esiea.poo.esieabay.core.dao.IDao;
import fr.esiea.poo.esieabay.core.dao.factory.DaoFactory;
import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.model.implementation.User;

public class UserDaoService implements IDaoService<User> {

	private IDao<User> dao;


	public UserDaoService() {
		this.dao = DaoFactory.getUserDao();
	}

	@Override
	public void create(User user) throws NonUniqueException {
		if(dao.get(user.getLogin()) !=null)
			throw new NonUniqueException("This login already exist");
		dao.create(user);
	}

	@Override
	public User get(String id) {
		return dao.get(id);
	}


	@Override
	public void update(User user) {
		dao.update(user);
	}

	@Override
	public Collection<User> getAll() {
		return dao.get();
	}

}

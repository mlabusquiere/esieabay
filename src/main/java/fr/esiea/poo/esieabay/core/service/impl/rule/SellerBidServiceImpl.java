package fr.esiea.poo.esieabay.core.service.impl.rule;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.BidNotUpdatableException;
import fr.esiea.poo.esieabay.core.exception.NoExistException;
import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.exception.PublishedException;
import fr.esiea.poo.esieabay.core.exception.UncancellableBidException;
import fr.esiea.poo.esieabay.core.exception.WrongFormatException;
import fr.esiea.poo.esieabay.core.service.IBidSellerService;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.composant.bidenum.BidState;
import fr.esiea.poo.esieabay.model.imodel.IBid;
import fr.esiea.poo.esieabay.model.imodel.ISeller;
import fr.esiea.poo.esieabay.model.imodel.ModelFacade;


public class SellerBidServiceImpl implements IBidSellerService{

	private IDaoService<IBid> bidDao;

	public SellerBidServiceImpl() {
		super();
		this.bidDao = ServiceFactory.getBidDaoService();
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.core.service.IBidSellerService#makeBid(fr.esiea.poo.esieabay.model.role.Seller, fr.esiea.poo.esieabay.model.Bid)
	 */
	@Override
	public void makeBid(ISeller userAuthenticated, IBid bid) throws NonUniqueException, WrongFormatException, AuthenticationException {
		if(bid == null) {
			return ; //TODO exception ??
		}

		checkAuthentication(userAuthenticated,bid.getSeller());

		if(bidDao.get(bid.getId()) == null)	{
			userAuthenticated.createBid(bid);
		}
		else {
			throw new NonUniqueException("This bid already exist");
		}

	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.core.service.IBidSellerService#publishBid(fr.esiea.poo.esieabay.model.role.Seller, java.lang.String)
	 */
	@Override
	public void publishBid(ISeller userAuthenticated, String idBid) throws NoExistException, PublishedException, AuthenticationException {
		IBid bid = bidDao.get(idBid);

		if(bid == null) {
			throw new NoExistException("This bid does not exist");
		}
		checkAuthentication(userAuthenticated,bid.getSeller());

		userAuthenticated.publishBid(bid);
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.core.service.IBidSellerService#cancelBid(fr.esiea.poo.esieabay.model.role.Seller, java.lang.String)
	 */
	@Override
	public void cancelBid(ISeller userAuthenticated, String idBid) throws NoExistException, AuthenticationException, UncancellableBidException {
		IBid bid = bidDao.get(idBid);

		if(bid == null) {
			throw new NoExistException("This bid does not exist");
		}

		checkAuthentication(userAuthenticated,bid.getSeller());

		userAuthenticated.cancelBid(bid);
		bid.cancelledWarningNotify();
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.core.service.IBidSellerService#updateBid(fr.esiea.poo.esieabay.model.role.Seller, fr.esiea.poo.esieabay.model.Bid)
	 */
	@Override
	public void updateBid(ISeller userAuthenticated, IBid bid) throws AuthenticationException, BidNotUpdatableException {
		checkAuthentication(userAuthenticated,bid.getSeller());

		if(bid.getState() != BidState.CREATED) {
			throw new BidNotUpdatableException();
		}

		bidDao.update(bid);
	}



	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.core.service.IBidSellerService#getBids(fr.esiea.poo.esieabay.model.implementation.User)
	 */
	@Override
	public Collection<IBid> getBids(ISeller userAuthenticated) {

		Collection<IBid> bids = bidDao.getAll();
		if(bids == null)
			return new HashSet<IBid>();
		Collection<IBid> bidsTmp = new HashSet<IBid>(bids); 
		Iterator<IBid> iterator = bidsTmp.iterator();
		while(iterator.hasNext())	{
			IBid bid = iterator.next();

			if(! bid.getSeller().equals(userAuthenticated))	{
				bids.remove(bid);
			}
		}
		return bids;
	}

	@Override
	public IBid getBid(ISeller userAuthenticated, String id) throws AuthenticationException, NoExistException {
		IBid bid = bidDao.get(id);
		if(bid == null)
			throw new NoExistException("This bid does not exist");
		checkAuthentication(userAuthenticated, bid.getSeller());

		return bid;
	}
	/**
	 * Permet d'authentifier l'utilisateur avec celui qui a pass� l'enchere
	 * @param userAuthenticated Utilisateur connect�
	 * @param userOwner 		Le vendeur de la bid
	 * @throws AuthenticationException
	 */
	private void checkAuthentication(ModelFacade userAuthenticated, ModelFacade userOwner) throws AuthenticationException	{
		if(userAuthenticated == null)
			throw new AuthenticationException("You need to be authenticated");
		if(!userAuthenticated.equals(userOwner))
			throw new AuthenticationException("You are not owning this bid");
	}


}

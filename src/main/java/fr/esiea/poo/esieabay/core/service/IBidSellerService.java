package fr.esiea.poo.esieabay.core.service;

import java.util.Collection;

import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.BidNotUpdatableException;
import fr.esiea.poo.esieabay.core.exception.NoExistException;
import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.exception.PublishedException;
import fr.esiea.poo.esieabay.core.exception.UncancellableBidException;
import fr.esiea.poo.esieabay.core.exception.WrongFormatException;
import fr.esiea.poo.esieabay.model.imodel.IBid;
import fr.esiea.poo.esieabay.model.imodel.ISeller;

/**
 * Service propos� � l'api
 *
 */
public interface IBidSellerService {
	
	/**
	 * 
	 * Permet � un utilisteur authentifi� de faire une ench�re, le statut de l'ench�re sera CREATED 
	 * @param userAuthenticated 	Un utilisateur qui a �t� pr�alablement authentifi�
	 * @param bid					L'ench�re � cr�er
	 * @throws AuthenticationException 
	 * 
	 */
	public void makeBid(ISeller userAuthenticated,IBid bid) throws NonUniqueException,WrongFormatException, AuthenticationException;
	
	
	/**
	 * 
	 * Permet � un utilisateur authentifi� de publi� une ench�re 
	 * @param userAuthenticated 	Un utilisateur qui a �t� pr�alablement authenitifi�
	 * @param idBid					L'id de l'ench�re � publier
	 * @throws AuthenticationException 
	 * 
	 */
	public void publishBid(ISeller userAuthenticated, String idBid) throws NoExistException,PublishedException, AuthenticationException;
	

	/**
	 * 
	 * Permet � un utilisateur authentifi� d'annuler une ench�re 
	 * @param userAuthenticated 	Un utilisateur qui a �t� pr�alablement authenitifi�
	 * @param idBid					L'id de l'ench�re � annuler
	 * @throws NoExistException 
	 * @throws AuthenticationException 
	 * @throws UncancellableBidException 
	 * 
	 */
	public void cancelBid(ISeller userAuthenticated, String idBid) throws NoExistException, AuthenticationException, UncancellableBidException;
	
	/**
	 * 
	 * Permet � un utilisateur authentifi� de mettre � jour une ench�re 
	 * @param userAuthenticated 	Un utilisateur qui a �t� pr�alablement authenitifi�
	 * @param bid					L'ench�re mis a jour 
	 * @throws AuthenticationException 
	 * @throws BidNotUpdatableException 
	 * 
	 */
	public void updateBid(ISeller userAuthenticated, IBid bid) throws AuthenticationException, BidNotUpdatableException;
	
	
	/**
	 * 
	 * Permet � un utilisateur authentifi� de voir toutes ses ench�res
	 * @param userAuthenticated 	Un utilisateur qui a �t� pr�alablement authentifi�
	 * @return Collection<Bid>
	 * 
	 */
	public Collection<IBid> getBids(ISeller user);


	/**
	 * Permet a un vendeur de voir une de ses ench�re
	 * @param userAuthenticated
	 * @param id
	 * @return
	 * @throws AuthenticationException
	 * @throws NoExistException 
	 */
	public IBid getBid(ISeller userAuthenticated, String id) throws AuthenticationException, NoExistException;

}

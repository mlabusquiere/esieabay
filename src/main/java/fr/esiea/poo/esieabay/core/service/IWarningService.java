package fr.esiea.poo.esieabay.core.service;

import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.NoExistException;
import fr.esiea.poo.esieabay.model.composant.Warning;
import fr.esiea.poo.esieabay.model.imodel.IBuyer;
import fr.esiea.poo.esieabay.model.imodel.ISeller;


/**
 * Service propos� � l'api
 *
 */
public interface IWarningService {
	
	/**
	 * @param userAuthenticated
	 * @param stateWarning
	 * @param idBid
	 * @throws NoExistException
	 * @throws AuthenticationException
	 */
	public void reservePriceWarning(IBuyer buyer, boolean stateWarning, String idBid) throws NoExistException, AuthenticationException;
	
	/**
	 * @param userAuthenticated
	 * @param stateWarning
	 * @param idBid
	 * @throws NoExistException
	 * @throws AuthenticationException
	 */
	public void cancelledWarning(IBuyer buyer, boolean stateWarning, String idBid) throws NoExistException, AuthenticationException;
	
	/**
	 * @param userAuthenticated
	 * @param stateWarning
	 * @param idBid
	 * @throws NoExistException
	 * @throws AuthenticationException
	 */
	public void higherOfferWarning(IBuyer buyer, boolean stateWarning, String idBid) throws NoExistException, AuthenticationException;
	
	/**
	 * @param userAuthenticated
	 * @param warning
	 */
	public void removeAlert(ISeller seller, Warning warning);
	
	/**
	 * @param userAuthenticated
	 * @param warning
	 */
	public void removeAlert(IBuyer buyer, Warning warning);
	
}

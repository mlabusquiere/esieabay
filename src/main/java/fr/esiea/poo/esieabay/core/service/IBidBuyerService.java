package fr.esiea.poo.esieabay.core.service;

import java.util.Collection;

import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.ForbiddenException;
import fr.esiea.poo.esieabay.core.exception.NoExistException;
import fr.esiea.poo.esieabay.core.exception.NotPublishedException;
import fr.esiea.poo.esieabay.core.exception.OfferTooLowException;
import fr.esiea.poo.esieabay.core.exception.OwnBidException;
import fr.esiea.poo.esieabay.core.exception.PublishedException;
import fr.esiea.poo.esieabay.model.imodel.IBidProtected;
import fr.esiea.poo.esieabay.model.imodel.IBuyer;



/**
 * Service propos� � l'api
 *
 */
public interface IBidBuyerService {
	
	
	/**
	 * @param userAuthenticated
	 * @param bidId
	 * @param price
	 * @throws NoExistException
	 * @throws AuthenticationException
	 * @throws PublishedException
	 * @throws OwnBidException
	 * @throws NotPublishedException
	 * @throws OfferTooLowException
	 */
	public void makeOffer(IBuyer userAuthenticated, String bidId, float price) throws NoExistException, AuthenticationException, PublishedException, OwnBidException, NotPublishedException, OfferTooLowException;
	
	/**
	 * 
	 * Permet de voir toutes les ench�res publi�es (m�me non authentifi�)
	 * @return Collection<Bid>
	 * 
	 */
	public Collection<IBidProtected> getBids();

	/**
	 * Permet a un acheteur de voir une enchere si il el peut 
	 * @param id
	 * @return
	 * @throws NoExistException 
	 * @throws ForbiddenException 
	 */
	public IBidProtected getBid(String id) throws NoExistException, ForbiddenException;

	
	/**
	 * Toute les ench�res ou l'achetteur a fait une offre
	 * @param userAuthenticated
	 * @return
	 */
	Collection<IBidProtected> getBids(IBuyer userAuthenticated);
}

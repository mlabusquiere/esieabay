package fr.esiea.poo.esieabay.core.service.impl.dao;

import java.util.Collection;

import fr.esiea.poo.esieabay.core.dao.IDao;
import fr.esiea.poo.esieabay.core.dao.factory.DaoFactory;
import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.model.imodel.IBid;

public class BidDaoService implements IDaoService<IBid> {
	
	private IDao<IBid> dao;



	public BidDaoService() {
		super();
		this.dao = DaoFactory.getBidDao();
	}

	@Override
	public void create(IBid data) throws NonUniqueException {
		if(dao.get(data.getId()) !=null)
			throw new NonUniqueException("This id already exist");
		dao.create(data);
		
	}

	@Override
	public IBid get(String id) {
		return dao.get(id);
	}

	@Override
	public void update(IBid bid) {
		if(dao.get(bid.getId()) != null)
			dao.update(bid);
		//can't be update what do should we do?
		
	}

	@Override
	public Collection<IBid> getAll() {
		return dao.get();
	}

}

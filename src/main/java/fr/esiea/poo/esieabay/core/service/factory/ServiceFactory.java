package fr.esiea.poo.esieabay.core.service.factory;

import fr.esiea.poo.esieabay.core.service.IBidBuyerService;
import fr.esiea.poo.esieabay.core.service.IBidSellerService;
import fr.esiea.poo.esieabay.core.service.IWarningService;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.impl.dao.BidDaoService;
import fr.esiea.poo.esieabay.core.service.impl.dao.UserDaoService;
import fr.esiea.poo.esieabay.core.service.impl.rule.BuyerBidServiceImpl;
import fr.esiea.poo.esieabay.core.service.impl.rule.SellerBidServiceImpl;
import fr.esiea.poo.esieabay.core.service.impl.rule.WarningServiceImpl;
import fr.esiea.poo.esieabay.model.imodel.IBid;
import fr.esiea.poo.esieabay.model.implementation.User;

public class ServiceFactory {
	//DAO Service
	public static IDaoService<User> getUserService() {
		return new UserDaoService();
	}

	public static IDaoService<IBid> getBidDaoService() {
		return new BidDaoService();
	}
	//rule Service
	public static IBidSellerService getBidSellerService() {
		return new SellerBidServiceImpl();
	}
	
	public static IBidBuyerService getBidBuyerService() {
		return new BuyerBidServiceImpl();
	}

	public static IWarningService getWarningService() {
		return new WarningServiceImpl();
	}
}

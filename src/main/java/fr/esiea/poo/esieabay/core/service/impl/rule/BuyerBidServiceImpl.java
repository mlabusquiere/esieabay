package fr.esiea.poo.esieabay.core.service.impl.rule;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.ForbiddenException;
import fr.esiea.poo.esieabay.core.exception.NoExistException;
import fr.esiea.poo.esieabay.core.exception.NotPublishedException;
import fr.esiea.poo.esieabay.core.exception.OfferTooLowException;
import fr.esiea.poo.esieabay.core.exception.OwnBidException;
import fr.esiea.poo.esieabay.core.exception.PublishedException;
import fr.esiea.poo.esieabay.core.service.IBidBuyerService;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.composant.Offer;
import fr.esiea.poo.esieabay.model.composant.bidenum.BidState;
import fr.esiea.poo.esieabay.model.imodel.IBid;
import fr.esiea.poo.esieabay.model.imodel.IBidProtected;
import fr.esiea.poo.esieabay.model.imodel.IBuyer;
import fr.esiea.poo.esieabay.model.imodel.ModelFacade;

public class BuyerBidServiceImpl implements IBidBuyerService{

	private IDaoService<IBid> bidDao;


	public BuyerBidServiceImpl() {
		super();
		this.bidDao = ServiceFactory.getBidDaoService();
	}


	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.core.service.IBidBuyerService#makeOffer(fr.esiea.poo.esieabay.model.role.Buyer, java.lang.String, float)
	 */
	@Override
	public void makeOffer(IBuyer userAuthenticated, String bidId, float price) throws NoExistException, AuthenticationException, PublishedException, OwnBidException, NotPublishedException, OfferTooLowException {
		IBidProtected bid = bidDao.get(bidId);

		if(bid == null)
			throw new NoExistException("This bid does not exist");
		checkAuthentication(userAuthenticated);

		if(bid.getSeller().equals(userAuthenticated))
			throw new OwnBidException("You are owning this bid! Keep it if you don't want to sell it..");

		if( ! bid.getState().equals(BidState.PUBLISHED))
			throw new NotPublishedException("This bid is not published or is terminated");

		userAuthenticated.addOffer(bid, price);

	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.core.service.IBidBuyerService#getBids()
	 */
	@Override
	public Collection<IBidProtected> getBids() {

		Set<IBidProtected> bidsToReturn = new HashSet<IBidProtected>();

		Collection<IBid> bids = bidDao.getAll();
		Iterator<IBid> iterator = bids.iterator();

		while(iterator.hasNext())	{
			IBid bid = iterator.next();

			if(bid.getState().equals(BidState.PUBLISHED))	{
				bidsToReturn.add((IBidProtected)bid);
			}
		}

		if(bidsToReturn.isEmpty())
			return null;
		return bidsToReturn;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.core.service.IBidBuyerService#getBids()
	 */
	@Override
	public Collection<IBidProtected> getBids(IBuyer userAuthenticated) {

		Set<IBidProtected> bidsToReturn = new HashSet<IBidProtected>();

		Collection<IBid> bids = bidDao.getAll();
		Iterator<IBid> iterator = bids.iterator();

		while(iterator.hasNext())	{
			IBid bid = iterator.next();
			List<Offer> offers = bid.getOffers();
			if(!offers.isEmpty())
				for(Offer offer : offers)	{
					if(offer.getBuyer().equals(userAuthenticated))	{
						bidsToReturn.add(bid);
						break;
					}
				}

		}

		return bidsToReturn;
	}

	@Override
	public IBidProtected getBid(String id) throws NoExistException, ForbiddenException {
		IBid bid = bidDao.get(id);
		if(bid == null)
			throw new NoExistException("This bid does not exist");
		if(bid.getState().equals(BidState.PUBLISHED))
			return (IBidProtected)bid;
		else	{
			throw new ForbiddenException("You don't have the right to see this bid!");
		}
	}

	/**
	 * Permet de verifier qu'un utilisateur est connect�
	 * @param userAuthenticated
	 * @param userOwner
	 * @throws AuthenticationException
	 */
	private void checkAuthentication(ModelFacade userAuthenticated) throws AuthenticationException	{
		if(userAuthenticated == null)
			throw new AuthenticationException("You need to be authenticated");
	}
}

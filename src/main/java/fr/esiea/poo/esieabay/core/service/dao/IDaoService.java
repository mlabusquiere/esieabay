package fr.esiea.poo.esieabay.core.service.dao;

import java.util.Collection;

import fr.esiea.poo.esieabay.core.exception.NonUniqueException;


public interface IDaoService<T> {
	
	/**
	 * Permet de cr�er une data de type {T}
	 * @param data
	 * @throws NonUniqueException
	 */
	public void create(T data) throws NonUniqueException;

	/**
	 * Permet d'aller r�cuperer une data
	 * @param Id id de la data
	 * @return Data avec l'id id
	 */
	public T get(String id);

	/**
	 * Permet de mettre a jour une data dans la bdd
	 * @param data la data � metre a jour authentifi� par l'id
	 */
	public void update(T data);

	/**
	 * Permet d'obtenir toute les data du m�me type dans la abse de donn�e
	 * @return toute les data
	 */
	public Collection<T> getAll();
}

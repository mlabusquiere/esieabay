package fr.esiea.poo.esieabay.core.service.impl.rule;

import fr.esiea.poo.esieabay.core.exception.AuthenticationException;
import fr.esiea.poo.esieabay.core.exception.NoExistException;
import fr.esiea.poo.esieabay.core.service.IWarningService;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.composant.Warning;
import fr.esiea.poo.esieabay.model.imodel.IBid;
import fr.esiea.poo.esieabay.model.imodel.IBuyer;
import fr.esiea.poo.esieabay.model.imodel.ISeller;
import fr.esiea.poo.esieabay.model.imodel.ModelFacade;

public class WarningServiceImpl implements IWarningService {

	private IDaoService<IBid> bidDao;

		
	public WarningServiceImpl() {
		super();
		this.bidDao = ServiceFactory.getBidDaoService();
	}

	@Override
	public void reservePriceWarning(IBuyer userAuthenticated, boolean stateWarning, String idBid) throws NoExistException, AuthenticationException {

		IBid bid = bidDao.get(idBid);
		if(bid == null) {
			throw new NoExistException("This bid does not exist");
		}
		checkAuthentication(userAuthenticated,bid.getSeller());
		userAuthenticated.setReservedPriceWarning(idBid,stateWarning);
		
		if(stateWarning)	{
			bid.addObserver(userAuthenticated);
		}else	{
			bid.deleteObserver(userAuthenticated);
		}
	}

	@Override
	public void cancelledWarning(IBuyer userAuthenticated, boolean stateWarning, String idBid) throws NoExistException, AuthenticationException {
		IBid bid = bidDao.get(idBid);
		
		if(bid == null) {
			throw new NoExistException("This bid does not exist");
		}
		checkAuthentication(userAuthenticated, bid.getSeller());
		userAuthenticated.setCancelledWarning(idBid,stateWarning);
		
		if(stateWarning) {
			bid.addObserver(userAuthenticated);
		}else	{
			bid.deleteObserver(userAuthenticated);
		}
		
	}

	@Override
	public void higherOfferWarning(IBuyer userAuthenticated, boolean stateWarning, String idBid) throws NoExistException, AuthenticationException {
		IBid bid = bidDao.get(idBid);
		
		if(bid == null) {
			throw new NoExistException("This bid does not exist");
		}
		checkAuthentication(userAuthenticated,bid.getSeller());
		
		userAuthenticated.setHigherOffer(idBid,stateWarning);
		
		if(stateWarning)	{
			bid.addObserver(userAuthenticated);
		}else	{
			bid.deleteObserver(userAuthenticated);
		}
	}

	@Override
	public void removeAlert(IBuyer userAuthenticated, Warning warning) {
		userAuthenticated.removeWarning(warning);
	}

	@Override
	public void removeAlert(ISeller userAuthenticated, Warning warning) {
		userAuthenticated.removeWarning(warning);
	}
	
	private void checkAuthentication(ModelFacade userAuthenticated, ModelFacade userOwner) throws AuthenticationException	{
		if(userAuthenticated == null)
			throw new AuthenticationException("You need to be authenticated");
		if(userAuthenticated.equals(userOwner))
			throw new AuthenticationException("You are owning this bid");
	}

}

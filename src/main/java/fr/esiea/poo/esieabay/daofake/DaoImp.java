package fr.esiea.poo.esieabay.daofake;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import fr.esiea.poo.esieabay.core.dao.IDao;
import fr.esiea.poo.esieabay.model.imodel.ModelFacade;

public class DaoImp<T extends ModelFacade> implements IDao<T> {
	
	private Map<String,T> db;

	public DaoImp() {
		db = new HashMap<String,T>();
	}

	@Override
	public void create(T arg0) {
		//TODO handle id= null;
		db.put(arg0.getId(),arg0);
	
	}

	@Override
	public T get(String id) {
		
		return db.get(id);
		
	}

	@Override
	public void update(T arg0) {
		db.put(arg0.getId(),arg0);
		
	}

	@Override
	public void reset() {
		db = new HashMap<String,T>();
	}

	@Override
	public Collection<T> get() {
		return new HashSet<T>(db.values());
	}

}

package fr.esiea.poo.esieabay.model.implementation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

import fr.esiea.poo.esieabay.model.comparator.offer.PriceComparator;
import fr.esiea.poo.esieabay.model.composant.Date;
import fr.esiea.poo.esieabay.model.composant.Offer;
import fr.esiea.poo.esieabay.model.composant.Product;
import fr.esiea.poo.esieabay.model.composant.bidenum.BidState;
import fr.esiea.poo.esieabay.model.composant.bidenum.WarningType;
import fr.esiea.poo.esieabay.model.imodel.IBid;
import fr.esiea.poo.esieabay.model.imodel.ISeller;

public class Bid extends IBid implements Observer {
	
	private ISeller seller;
	private Product product;
	private Date dueDate;
	private float minPrice, reservePrice;
	private List<Offer> offers;
	private BidState state;
	private String id;

	public Bid(ISeller seller, Product product, Date dueDate) {
		super();
		this.id = UUID.randomUUID().toString();
		this.seller = seller;
		this.product = product;
		this.dueDate = dueDate;
		this.offers = new ArrayList<Offer>();
		this.minPrice = 0;
		this.reservePrice = -1;
		this.state = BidState.CREATED;
		this.addObserver(seller);
		if(dueDate != null)
			this.dueDate.addObserver(this);
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.ibidProtected#getMinPrice()
	 */
	@Override
	public float getMinPrice() {
		return minPrice;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.Ibid#setMinPrice(float)
	 */
	@Override
	public void setMinPrice(float minimumPrice) {
		this.minPrice = minimumPrice;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.Ibid#getReservePrice()
	 */
	@Override
	public float getReservePrice() {
		return reservePrice;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.Ibid#setReservePrice(float)
	 */
	@Override
	public void setReservePrice(float reservePrice) {
		this.reservePrice = reservePrice;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.ibidProtected#getState()
	 */
	@Override
	public BidState getState() {
		return state;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.Ibid#setState(fr.esiea.poo.esieabay.model.composant.bidenum.BidState)
	 */
	@Override
	public void setState(BidState state) {
		this.state = state;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.ibidProtected#getProduct()
	 */
	@Override
	public Product getProduct() {
		return product;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.ibidProtected#getDueDate()
	 */
	@Override
	public Date getDueDate() {
		return dueDate;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.ibidProtected#getOffers()
	 */
	@Override
	public List<Offer> getOffers() {
		return offers;
	}
	
	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.ibidProtected#getSeller()
	 */
	@Override
	public ISeller getSeller() {
		return seller;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.ibidProtected#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.ibidProtected#getMaxOffer()
	 */
	@Override
	public Offer getMaxOffer() {
		if(offers.isEmpty())
			return null;
		Collections.sort(offers, new PriceComparator());
		return offers.get(0);
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.ibidProtected#toString()
	 */
	@Override
	public String toString() {
		return "Bid [seller=" + seller + ", product=" + product + ", dueDate="
				+ dueDate + ", offers=" + offers + ", minPrice=" + minPrice
				+ ", reservePrice=" + reservePrice + ", state=" + state
				+ ", id=" + id + "]";
	}

	/* (non-Javadoc)
	 * @see fr.esiea.poo.esieabay.model.ibidProtected#addOffer(fr.esiea.poo.esieabay.model.composant.Offer)
	 */
	@Override
	public void addOffer(Offer offer) {
		//Notifie le vendeur
		setChanged();
		seller.update(this,WarningType.OFFER_PROPOSED);	
		clearChanged();
		//Ntifie les autres achteur si le prix de reserve a �t� atteint ou pas
		if(offer.getPrice()>reservePrice)	{
			setChanged();
			notifyObservers(WarningType.RESERVED_PRICE);
		}
		//Notifie les autres acheteurs si il y a eu une meillieur offre
		Collections.sort(offers, new PriceComparator());
		
		for(int i=offers.size()-1 ; i>=0; i--)	{
			Offer offerInc = offers.get(i);
			if(offerInc.getPrice()<offer.getPrice())	{
				if(offerInc.getBuyer()!=offer.getBuyer())
					offerInc.getBuyer().update(this,WarningType.HIGHER_OFFER);
			}else	{
				break; //TODO discuss about optimisation
			}
		}
		
		offers.add(offer);
	}

	public void cancelledWarningNotify() {
			setChanged();
			notifyObservers(WarningType.CANCELLED);
			
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		if(arg1 instanceof Date)	{
			if(dueDate.isAfter((Date) arg1))	{
				state = BidState.FINISHED;
			}
		}
		
	}

}

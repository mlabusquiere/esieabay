package fr.esiea.poo.esieabay.model.imodel;

import fr.esiea.poo.esieabay.model.composant.bidenum.BidState;

public abstract class IBid extends IBidProtected{

	public abstract void setMinPrice(float minimumPrice);

	public abstract float getReservePrice();

	public abstract void setReservePrice(float reservePrice);

	public abstract void setState(BidState state);
	
	public abstract void cancelledWarningNotify();
	
}
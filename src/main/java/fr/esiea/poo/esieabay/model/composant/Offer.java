package fr.esiea.poo.esieabay.model.composant;

import fr.esiea.poo.esieabay.model.imodel.IBuyer;


public class Offer {
	
	private float price;
	private IBuyer buyer;
	
	public Offer(float price, IBuyer buyer) {
		super();
		this.setPrice(price);
		this.setBuyer(buyer);
	}

	public IBuyer getBuyer() {
		return buyer;
	}

	public void setBuyer(IBuyer buyer) {
		this.buyer = buyer;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		if(price < 0)
			price = 0;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Offer [price=" + price + ", buyer=" + buyer + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buyer == null) ? 0 : buyer.hashCode());
		result = prime * result + Float.floatToIntBits(price);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Offer other = (Offer) obj;
		if (buyer == null) {
			if (other.buyer != null)
				return false;
		} else if (!buyer.equals(other.buyer))
			return false;
		if (Float.floatToIntBits(price) != Float.floatToIntBits(other.price))
			return false;
		return true;
	}
	
	

}

package fr.esiea.poo.esieabay.model.composant.bidenum;

public enum WarningType {
	RESERVED_PRICE,
	CANCELLED,
	HIGHER_OFFER,
	OFFER_PROPOSED;
}

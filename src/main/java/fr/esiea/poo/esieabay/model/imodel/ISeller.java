package fr.esiea.poo.esieabay.model.imodel;

import java.util.Collection;
import java.util.Observer;

import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.exception.PublishedException;
import fr.esiea.poo.esieabay.core.exception.UncancellableBidException;
import fr.esiea.poo.esieabay.core.exception.WrongFormatException;
import fr.esiea.poo.esieabay.model.composant.Warning;

public interface ISeller extends ModelFacade,Observer {

	public void createBid(IBid bid) throws WrongFormatException, NonUniqueException;
	public void publishBid(IBid bid) throws PublishedException;
	public void cancelBid(IBid bid) throws UncancellableBidException;
	public Collection<Warning> getWarnings();
	public void removeWarning(Warning warning);
}

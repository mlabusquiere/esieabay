package fr.esiea.poo.esieabay.model.implementation.role;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;

import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.composant.Warning;
import fr.esiea.poo.esieabay.model.composant.bidenum.WarningType;
import fr.esiea.poo.esieabay.model.imodel.IBuyer;
import fr.esiea.poo.esieabay.model.imodel.ISeller;
import fr.esiea.poo.esieabay.model.implementation.Bid;
import fr.esiea.poo.esieabay.model.implementation.User;

public abstract class WarningUserSytemImpl implements ISeller, IBuyer {

	private List<Warning> warnings;
	private Map<String,Map<WarningType,Boolean>> warningOptions;
	private IDaoService<User> userDao;
	
	public WarningUserSytemImpl()	{
		warnings = new ArrayList<Warning>();
		warningOptions = new HashMap<String, Map<WarningType,Boolean>>();
		userDao = ServiceFactory.getUserService();
	}
	@Override
	public Collection<Warning> getWarnings() {
		return warnings;
	}

	@Override
	public void removeWarning(Warning warning) {
		warnings.remove(warning);
	}

	@Override
	public void setReservedPriceWarning(String idBid,boolean stateWarning)	{
		setWarningOption(idBid, stateWarning, WarningType.RESERVED_PRICE);
	}


	@Override
	public void setCancelledWarning(String idBid, boolean stateWarning)	{
		setWarningOption(idBid, stateWarning, WarningType.CANCELLED);
	}

	@Override
	public void setHigherOffer(String idBid, boolean stateWarning)	{
		setWarningOption(idBid, stateWarning, WarningType.HIGHER_OFFER);
	}
	
	private void setWarningOption(String idBid,boolean stateWarning,WarningType type)	{
		if(warningOptions.get(idBid) == null)	{
			warningOptions.put(idBid, innitMapOptions());
		}
		Map<WarningType, Boolean> mapOptions = warningOptions.get(idBid);
		mapOptions.put(type,stateWarning);
		warningOptions.put(idBid, mapOptions);
	}
	private Map<WarningType, Boolean> innitMapOptions() {
		Map<WarningType, Boolean> innitMapOptions = new HashMap<WarningType, Boolean>();
		innitMapOptions.put(WarningType.CANCELLED, false);
		innitMapOptions.put(WarningType.RESERVED_PRICE, false);
		innitMapOptions.put(WarningType.HIGHER_OFFER, false);
		return innitMapOptions;
	}


	@Override
	public void update(Observable obs, Object arg)	{
		if(obs instanceof Bid)	{
			String id = ((Bid) obs).getId();
			Map<WarningType, Boolean> map = warningOptions.get(id);
			if(map == null && (WarningType) arg != WarningType.OFFER_PROPOSED)	{
				return ;//TODO IMproove (may exception)
				
			}
				
			switch((WarningType) arg)	{
			case OFFER_PROPOSED : 
				warnings.add(new Warning("A new offer is on your bid n�" + id)); 
				userDao.update((User)this);
				break;
			case HIGHER_OFFER :
				if(map.get(WarningType.HIGHER_OFFER))	{
					warnings.add(new Warning("There is an offer higher than you on the object n�"+id));
					userDao.update((User)this);
				}
				break;
			case CANCELLED : 
				if(map.get(WarningType.CANCELLED))	{
					warnings.add(new Warning("The offer n�" + id +" was cancelled"));
					userDao.update((User)this);
				}
				break;
			case RESERVED_PRICE : 
				if(map.get(WarningType.RESERVED_PRICE))	{
					warnings.add(new Warning("An offer is higher than the reserve price of the object n�"+id));
					userDao.update((User)this);
				}
				break;
			}
		}
	}
}

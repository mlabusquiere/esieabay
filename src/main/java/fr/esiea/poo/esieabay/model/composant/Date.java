package fr.esiea.poo.esieabay.model.composant;

import java.util.Observable;

public class Date extends Observable {
	private int date;
	//Only for test
	private static Date actualDate = new Date(0);
	
	public Date(int date) {
		this.setDate(date);
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		this.date = date;
	}

	@Override
	public String toString() {
		return "Date [date=" + date + "]";
	}

	public static Date getActualDate() {
		return actualDate;
	}
	//Only for test
	public void setActualDate(Date actualDate) {
		
		Date.actualDate = actualDate;
		setChanged();
		notifyObservers(actualDate);
	}

	public boolean isAfter(Date arg1) {
		if(arg1.getDate()<this.getDate())
			return false;
		return true;
	}

	
}
 
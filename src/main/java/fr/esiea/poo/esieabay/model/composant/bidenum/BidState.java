package fr.esiea.poo.esieabay.model.composant.bidenum;

public enum BidState {
	CREATED,
	PUBLISHED,
	CANCELLED,
	FINISHED;
}

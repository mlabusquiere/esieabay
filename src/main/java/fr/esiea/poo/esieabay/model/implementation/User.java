package fr.esiea.poo.esieabay.model.implementation;

import fr.esiea.poo.esieabay.model.implementation.role.RoleUserImpl;

public class User extends RoleUserImpl {	
	

	private String login, lastname, firstname;
	
	public User(String login, String lastname, String firstname) {
		super();
		this.login = login;
		this.lastname = lastname;
		this.firstname = firstname;
	}


	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		//TODO checkifunique
		this.login = login;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	@Override
	public String getId() {
		return getLogin();
	} 
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "User [login=" + login + ", lastname=" + lastname
				+ ", firstname=" + firstname + ", getLastname()="
				+ getLastname() + ", getLogin()=" + getLogin()
				+ ", getFirstname()=" + getFirstname() + ", getId()=" + getId()
				+ ", hashCode()=" + hashCode() + ", getAlerte()=" + getWarnings()
				+ ", getClass()=" + getClass() + ", toString()="
				+ super.toString() + "]";
	}

}

package fr.esiea.poo.esieabay.model.implementation.role;

import fr.esiea.poo.esieabay.core.exception.NonUniqueException;
import fr.esiea.poo.esieabay.core.exception.OfferTooLowException;
import fr.esiea.poo.esieabay.core.exception.PublishedException;
import fr.esiea.poo.esieabay.core.exception.UncancellableBidException;
import fr.esiea.poo.esieabay.core.exception.WrongFormatException;
import fr.esiea.poo.esieabay.core.service.dao.IDaoService;
import fr.esiea.poo.esieabay.core.service.factory.ServiceFactory;
import fr.esiea.poo.esieabay.model.composant.Offer;
import fr.esiea.poo.esieabay.model.composant.bidenum.BidState;
import fr.esiea.poo.esieabay.model.imodel.IBid;
import fr.esiea.poo.esieabay.model.imodel.IBidProtected;

public abstract class RoleUserImpl extends WarningUserSytemImpl  {

	private IDaoService<IBid> bidDao;

	public RoleUserImpl() {
		super();
		bidDao = ServiceFactory.getBidDaoService();
	}

	@Override
	public void createBid(IBid bid) throws WrongFormatException, NonUniqueException {

		if(bid.getProduct() == null)
			throw new WrongFormatException("Need a product");
		if(bid.getDueDate() == null)
			throw new WrongFormatException("Need a due date");
		bidDao.create(bid);
	}

	@Override
	public void publishBid(IBid bid) throws PublishedException {
		if(bid.getState()!= BidState.CREATED)
			throw new PublishedException("Can't publish this bid because its state is "+bid.getState().toString());
		bid.setState(BidState.PUBLISHED);
		bidDao.update(bid);
	}

	@Override
	public void cancelBid(IBid bid) throws UncancellableBidException {	

		if(bid.getState().equals(BidState.CREATED) 
				|| bid.getMaxOffer() == null 
				|| bid.getMaxOffer().getPrice()<bid.getReservePrice())	{
			bid.setState(BidState.CANCELLED);
			bidDao.update(bid);
		}
		else {
			throw new UncancellableBidException();
		}
	}

	@Override
	public void addOffer(IBidProtected bid, float price) throws OfferTooLowException	{
		if(bid.getMinPrice() <= price)
			bid.addOffer(new Offer(price,this));
		else
			throw new OfferTooLowException();
	}


}

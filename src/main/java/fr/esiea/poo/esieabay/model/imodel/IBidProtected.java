package fr.esiea.poo.esieabay.model.imodel;

import java.util.List;
import java.util.Observable;

import fr.esiea.poo.esieabay.model.composant.Date;
import fr.esiea.poo.esieabay.model.composant.Offer;
import fr.esiea.poo.esieabay.model.composant.Product;
import fr.esiea.poo.esieabay.model.composant.bidenum.BidState;

public abstract class IBidProtected extends Observable implements ModelFacade{

	public abstract float getMinPrice();

	public abstract BidState getState();

	public abstract Product getProduct();

	public abstract Date getDueDate();

	public abstract List<Offer> getOffers();

	public abstract ISeller getSeller();

	@Override
	public abstract String getId();

	public abstract Offer getMaxOffer();

	@Override
	public abstract String toString();

	public abstract void addOffer(Offer offer);

}
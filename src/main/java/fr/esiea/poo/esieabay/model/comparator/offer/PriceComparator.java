package fr.esiea.poo.esieabay.model.comparator.offer;

import java.util.Comparator;

import fr.esiea.poo.esieabay.model.composant.Offer;

public class PriceComparator implements Comparator<Offer> {

	@Override
	public int compare(Offer o1, Offer o2) {
		if(o1.getPrice()>o2.getPrice())
			return -1;
		if(o1.getPrice()<o2.getPrice())
			return 1;
		return 0;
	}

}

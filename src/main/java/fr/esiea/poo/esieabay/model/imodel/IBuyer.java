package fr.esiea.poo.esieabay.model.imodel;

import java.util.Collection;
import java.util.Observer;

import fr.esiea.poo.esieabay.core.exception.OfferTooLowException;
import fr.esiea.poo.esieabay.model.composant.Warning;

public interface IBuyer extends ModelFacade, Observer	{

	public void addOffer(IBidProtected bid, float price) throws OfferTooLowException;
	public Collection<Warning> getWarnings();
	public void removeWarning(Warning warning);
	public void setReservedPriceWarning(String IdBid, boolean stateWarning);
	public void setCancelledWarning(String IdBid, boolean stateWarning);
	public void setHigherOffer(String IdBid, boolean stateWarning);
}
